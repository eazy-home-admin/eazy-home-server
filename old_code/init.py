#!/usr/bin/env python3

"""
Backup of legacy code.
"""

from flask import Flask, redirect, url_for, render_template, request, session
from telegram_send import telegram_send

# import arduino_controller as arduino
from EazyDB.store import *
from AI import identify_face as rec
from datetime import datetime as dt
from PIL import Image
from Device.arduino.ArduinoController import Arduino

server = Flask(__name__, template_folder='template')


# Global constant
arduino: Arduino = None
s = None  # This variable is created for arduino
useAutoDoor = True
ServerTime = dt.now()


@server.route("/")
def index():
    # session["username"]="admin"
    # Un-comment the above line only for debugging purpose
    if "username" in session:  # Verify login
        try:
            device_list = tuple(list_devices_name_detailed())
            return render_template("index.html", x=device_list)
        except Exception as e:
            return render_template("index.html", error=e)
    else:
        return redirect(url_for("login"))


# setting / changing admin password:
# This will be changed in future update for now it is commented out.
@server.route("/change-admin-pass", methods=['GET', 'POST'])
def change_password():
    if "username" in session:
        if session["username"] == "admin":
            if request.method == "POST":
                p1 = request.form["password1"]
                p2 = request.form["password2"]
                if p1 == p2:
                    try:
                        change_password('admin', p1)
                    except Exception as e:
                        return render_template("change-admin-pass.html", error=e)
                else:
                    return render_template("change-admin-pass.html", error="password did not match")
    else:
        return redirect(url_for("login"))


# Settings page
@server.route("/settings", methods=['GET', 'POST'])
def settings_page():
    if "username" not in session:
        return redirect(url_for("login"))
    else:
        if request.method == "GET":
            config = getConfig()
            return render_template("settings.html", x=config)
        else:
            change_config(request.form['arduinoport'], request.form['debug'], request.form['port'],
                          request.form['use_telegram'], request.form['use_automatic_door'], request.form['use_https'])
            return redirect(url_for('index'))


@server.route('/addDevices', methods=['GET', 'POST'])
def add_device():
    if "username" not in session:
        return redirect(url_for("login"))
    else:
        a = list_devices_name()
        if request.method == 'POST':
            device_name = request.form['devname']
            pin = request.form['pinname']
            choice = request.form['choice']
            if choice == 'add':
                if device_name not in a:
                    add_devices(device_name, pin)
                    a = list_devices_name()
                    return render_template("addDevices.html", x=a)
                else:
                    return render_template('addDevices.html', error="The record already exist", x=a)
            else:
                if device_name not in a:
                    a = list_devices_name()
                    return render_template('addDevices.html', x=a, error='Invalid Record.')
                else:
                    remove_device(device_name)
                    a = list_devices_name()
                    return render_template("addDevices.html", x=a)
        else:
            # get request
            return render_template("addDevices.html", x=a)


# login page
@server.route("/login", methods=["POST", "GET"])
def login():
    if "username" in session:  # check if user is in the session
        return redirect(url_for("index", usr=session["username"]))
    else:
        try:
            if request.method == "POST":
                user = request.form['username']
                password = request.form['password']
                result = verify_user(user, password)
                if result is None:
                    return render_template("login.html", error="User name not found")
                elif result:
                    session["username"] = user
                    return redirect(url_for("index"))
                else:
                    return render_template("login.html", error="Wrong login Credentials!")
            else:
                return render_template("login.html")
        except Exception as e:
            return render_template("login.html", error=e)


@server.route("/api/unlock_door", methods=["GET", "POST"])
def api_process_image():
    if request.method == "POST":
        file = request.files['image']
        img = Image.open(file.stream)
        face = rec.classify_face_cached(img)
        conf = getConfig()

        # Send the image to telegram regardless of the face recognition result
        os.system('telegram-send --file \'res.jpg\' --caption \"log\"')

        # Handle the case where a face is not recognized
        if face is None or face[0] == 'Unknown':
            telegram_send.send(messages=["Someone tried to use the face recognition system"])
            face_status = 'Unknown'
        else:
            telegram_send.send(messages=[f"{face[0]} was let inside the home"])
            face_status = face[0]
            if conf.get('useAutoDoor'):
                door_command = "Door:Unlock" if conf.get('doorType') == 'legacy' else 'ul'
                arduino.write(door_command)

        # Return a JSON response
        return jsonify({
            'msg': 'Success',
            'size': [img.width, img.height],
            'face': face_status
        })
    return jsonify({'msg': 'Invalid request method'}), 405  # Method Not Allowed


@server.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("login"))


@server.route("/usage_summary/<date>")
def usage_summary(date):
    if date == 'present':
        graph_status = graph('cur')
        my_dat = dt.now()
        str_my_dat = my_dat.strftime('%m-%Y')
        if graph_status:
            return render_template('usage_summary.html', date=str_my_dat)
    else:
        print(type(date))
        graph_status = graph(str(date))
        if graph_status is True:
            return render_template('usage_summary.html', date=date)
        else:
            return 'Data not found'


# Main Code that consist of Devices command
@server.route("/device/<device_name>/<device_status>")
def device_toggle(device_name, device_status):
    conf = getConfig()
    try:
        if device_name not in list_devices_name():
            return jsonify({'Error': 'Device not found'})
        else:
            if conf[6] == "legacy":
                stuff, flag = arduino.write(f'{device_name}:{device_status}')
            else:
                pin: int = get_pin_number(device_name)
                if get_device_status(device_name) == "off":
                    stuff, flag = arduino.turn_on(pin)
                else:
                    stuff, flag = arduino.turn_off(pin)
            if flag:
                change_device_status(device_name, device_status)
                if stuff.find("error") or stuff.find("Error"):
                    return str(stuff)

    except Exception as e:
        return str(e)


@server.route('/lock_door')
def lock_door():
    conf = getConfig()
    if useAutoDoor:
        if conf[6] == 'legacy':
            arduino.write("Door:Lock")
        else:
            arduino.write('Door:Unlock')
        return 'Door has been locked.'
    else:
        return 'Door Lock has been Disabled'


# API Call Request
@server.route("/api/device/<device_name>/<device_status>")
def api_device_toggle(device_name, device_status):
    config = getConfig()
    try:
        list_device = list_devices_name()
        if device_name not in list_device:
            return jsonify({'Error': 'Device not found'})
        else:
            print("Mode is set to: ", config[6])
            if config[6] == 'legacy':
                stuff, flag = arduino.write(f'{device_name}:{device_status}')
                change_device_status(device_name, device_status)
            elif config[6] == 'normal':
                pin: int = get_pin_number(device_name)
                if get_device_status(device_name) == "off":
                    stuff, _ = arduino.turn_on(pin)
                else:
                    stuff, _ = arduino.turn_off(pin)
                change_device_status(device_name, device_status)
            return api_status()
    except Exception as e:
        json_dict = {'error': str(e)}
        return jsonify(json_dict)


from flask import jsonify

@server.route("/api/status")
def api_status():
    try:
        # Use a dictionary comprehension to build json_dict
        json_dict = {name: status for name, status in get_device_status()}
        return jsonify(json_dict)  # Return with a 200 OK status code
    except Exception as e:
        # Log the exception for debugging purposes
        # Consider using a logging framework for logging errors
        print(e)
        # Return a JSON with error info and a 500 Internal Server Error status code
        return jsonify({"error": "An error occurred while fetching device status."}), 500



@server.route('/api/get_all_device_list')
def myDevicesList():
    x = list_devices_name()
    data = dict()
    for i in range(len(x)):
        data[str(i)] = x[i]
    return jsonify(data)


@server.route('/api/lock_door')
def api_lock_door():
    conf = getConfig()
    if useAutoDoor:
        if conf[6] == 'legacy':
            arduino.arduino_write(s, "Door:Lock")
        else:
            arduino.toggle_door(s, 'l')
        return jsonify({'msg': 'door has been locked'})
    else:
        return jsonify({'msg': 'Automatic Door is not supported'})


@server.route('/plot_graph', methods=['POST', 'GET'])
def plot_graph():
    if request.method == 'POST':
        date = str(request.form["date"])
        match = date.split('-')
        if len(match[0]) == 2 and len(match[1]) == 4:
            if os.path.exists(f'./static/_data/{date}.yaml'):
                return redirect(f'/usage_summary/{date}')
            else:
                return redirect('/usage_summary/present')
        else:
            return "pattern not matched"
    else:
        return render_template('plot_graph.html')


@server.route('/plot_regression')
def plot_regression():
    data, predict_day, predict_dates = showRegressionFor30days()
    tuple_data = tuple(data)
    return render_template('plotRegression.html', x=tuple_data, predDate=predict_dates, predDay=predict_day)


@server.route('/api/<device_name>/status')
def api_status_device(device_name):
    a = get_device_status(device_name)
    data = dict()
    data[device_name] = a
    return jsonify(data)


@server.route('/api/info/<device_name>')
def api_device_info(device_name):
    device_details = get_device_details(device_name)
    data = dict()
    data['device-info'] = device_details
    return jsonify(data)


# Error Handling Methods
@server.errorhandler(404)
def not_found(e):
    return "Webpage not found"


@server.errorhandler(403)
def permission_denied(e):
    return "Access Denied!"


@server.errorhandler(400)
def bad_request(e):
    return str(e)


@server.errorhandler(500)
def internal_error(e):
    return "Internal error: " + str(e)


@server.errorhandler(503)
def int_error(e):
    return "Internal error: " + str(e)


def main():
    global arduino
    print("Starting the app...")
    rec.train_face_cache()
    device_list = list_devices_name()
    data = dict()
    showRegressionFor30days()
    try:
        # Initial Checks
        if not os.path.exists('./config.yaml'):
            print("Warning: CONFIGURATION FILE NOT FOUND. FILLED WITH DEFAULT VALUES.")
            create_config_file()
        if not os.path.exists("./static/graphs"):
            os.mkdir('./static/graphs')
            print("graphs are stored in static/graphs")
        with open('config.yaml', 'r') as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        data['server_start_time'] = dt.now()
        with open('config.yaml', 'w') as f:
            yaml.dump(data, f)

        arduino = Arduino(data['arduinoport'], True)

        useAutoDoor = data['use-automated-door'] or False
        f.close()
    except:
        print("Could not connect to the Arduino device.")
        if not data['debug']:
            quit()

    try:
        # Determine the port
        port = 443 if data['use-https'] else 80  # Default ports for HTTPS and HTTP
        if data['port'] != 'default' or data['use-https']:
            port = int(data['port'])

        # Determine SSL context
        ssl_context = ('server.crt', 'server.key') if data['use-https'] else None

        # Run the app
        server.run(host='0.0.0.0', debug=bool(data['debug']), port=port, ssl_context=ssl_context, threaded=True)

    finally:
        device_list = list_devices_name()
        for device in device_list:
            arduino.write(device + ":" + "off")
        change_all_device_status("off")


if __name__ == "__main__":
    main()
