import transcriber as t
import dsparser as p
from Device.core import device as d
import wordDict as w

import TestUniAPI



light = d.PassiveDevice("light", ["color", "brightness", "effect"], TestUniAPI.api, '127.0.0.1', 12345)

devices = [light]

#Example prototype parser
parser = p.Parser(w.bloatWords, devices, w.uniDict)

#Example transcriber
transcriber = t.Transcriber("deepspeech-0.9.3-models.pbmm", "deepspeech-0.9.3-models.scorer")

text = transcriber.transcribeFile("untitled.wav")

print(text)

#Clean and unify the command
cleanedList = parser.prototypeCleanOutput(text)

command = parser.prototypeUnifier(cleanedList)

#Call the associated translation function
try:
    print(light.funcDict["ping"](light))
    command.subject.funcDict["main"](command)

except Exception as error:
    print(error)
