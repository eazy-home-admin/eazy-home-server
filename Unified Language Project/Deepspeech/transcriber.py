import deepspeech
import wave
import numpy
import pyaudio
import time


# REQUIRES PYAUDIO TO DO LOCAL DIRECT STREAMING


class Transcriber():
    def __init__(this, modelPath, scorerPath):
        # Create a deepspeech model
        this.model = deepspeech.Model(modelPath)

        # Enable scoring for the model
        this.model.enableExternalScorer(scorerPath)

        # Get the sample rate the model was trained on (for pre-trained 0.9.3 model 16000)
        this.modelSampleRate = this.model.sampleRate();

    # Function to transcribe a wave file
    def transcribeFile(this, filePath):
        try:

            # Open the desired file
            file = wave.open(filePath, 'r')

            # Do a check for the sample rate
            if (file.getframerate() != this.modelSampleRate):
                raise Exception("The sample rate of the provided file (", file.getframerate(),
                                ") does not match the sample rate of the model (", this.modelSampleRate, ")")

            else:

                # Get total number of frames in the file
                frames = file.getnframes()

                # Make a buffer object
                buffer = file.readframes(frames)

                # Make a deepspeech compatible stream buffer with numpy
                dsBuffer = numpy.frombuffer(buffer, dtype=numpy.int16)

                # Run the transcription and return the resulting text
                return this.model.stt(dsBuffer)

        except Exception as error:
            print('Caught this error: ' + repr(error))

    # Live function very unstable DO NOT USE YET:

    def transcribeLive(this):

        context = this.model.createStream()

        # Encapsulate DeepSpeech audio feeding into a callback for PyAudio
        def process_audio(in_data, frame_count, time_info, status):
            data16 = numpy.frombuffer(in_data, dtype=numpy.int16)
            context.feedAudioContent(data16)
            return (in_data, pyaudio.paContinue)

        # Feed audio to deepspeech in a callback to PyAudio
        audio = pyaudio.PyAudio()
        stream = audio.open(
            format=pyaudio.paInt16,
            channels=1,
            rate=this.modelSampleRate,
            input=True,
            frames_per_buffer=10240,
            stream_callback=process_audio
        )

        # Start the stream
        stream.start_stream()

        # Listen for three seconds
        x = 0
        while x < 300:
            time.sleep(0.01)
            x += 1

        # Transcribe
        text = context.finishStream()

        # Close pyaudio stream
        stream.stop_stream()
        stream.close()
        audio.terminate()

        return text
