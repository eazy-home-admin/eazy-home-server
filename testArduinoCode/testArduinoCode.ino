/*
* Arduino code
* pname consist of devices name 
* ppin has the pins of home devices
* fill in the details accordingly
* Only Passive device is now Supported. Soon active device support will be made into the server.
* This code is also Licensed under GPL license. You are free to use the code as you wish.
*/

#define PDEV 4
#define door 3

int ppin[PDEV] = {9, 10, 11, 12};
String pname[4] = {"lamp1", "lamp2", "fan1", "fan2"};

int pstate[] = {0, 0, 0, 0};
void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  for (int i = 0; i < 4 ; ++i) {
    pinMode(ppin[i], OUTPUT);
  }
}


void loop() {
  int flag = 0;
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) { // If serial port is ready to read
    String x = Serial.readString(); // read string from the serial port
    x.trim(); // trim spaces
    int loc =  x.indexOf(':'); // finding the index of ':'
    int len = (int) x.length(); // lenth of string x
    if (loc <=  0){
          Serial.println("Invalid Arugment supplied.");
    }
    else {

      String split[2]; // a variable to contain split values
      split[0] = x.substring(0, loc);
      split[1] = x.substring(loc + 1);
      if (split[0].equals("read")) {
        String dev = split[1];
        flag = 0;

      }
      else if (split[0].equals("Door")){
        if(split[1].equals("Unlock") ||split[1].equals("unlock") )
        digitalWrite(door, 1);
       else if(split[1].equals("Lock") ||split[1].equals("lock") )
        digitalWrite(door, 0);
      }
      else if (split[0].equals("test")){
        int flag = 1;
          for (int i = 0 ; i < PDEV; ++i) {
            if (pname[i] == split[0]){
              flag= 1;
              break;
              }
            else
              flag = 0;
          }
          if (flag == 1)
            Serial.println("Device Exists");
          else
            Serial.println("Device not Exist.");  
      }
      else {
        flag = 0;
        for (int i = 0 ; i < PDEV; ++i) {

          //Serial.println("DEBUG PNAME CONTENTS: " + pname[i]);
          if (pname[i] == split[0]) {
            if (split[1] == "on" && pstate[i] == 0) {
              digitalWrite(ppin[i], 1);
              Serial.println("Info: " + split[0] + " is turned on");
              pstate[i] = 1;
              flag = 1;
              break;
  
            }
            else if (split[1] == "off" && pstate[i] == 1) {
              digitalWrite(ppin[i], 0);
              Serial.println("Info: " + split[0] + " is turned off");
              pstate[i] = 0;
              flag = 1;
              break;
            }
            else {
              if (split[1] == "on" || split[1] == "off")
                Serial.println("Error:the devices is already turned " + split[1]);
              else
                Serial.println("Error: Invalid State;");

              flag = 1;
              break;
            }

          }
          else
            flag = 0;

        }
        if (flag == 0)
          Serial.println("Error: Invalid Device");

      }
    }
  }
}
