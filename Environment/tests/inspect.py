"""
Utility script to check if a word is a module or class in the current scope.
This is used for building safelibs
"""

import importlib
import inspect
import sys
def is_module_or_class(word):
    try:
        # Try to import the module
        module = importlib.import_module(word)
        return 'module'

    except ImportError:
        # If import fails, it might be a class
        pass

    try:
        # Check if the word is a class in the current scope
        obj = eval(word)
        if inspect.isclass(obj):
            return 'class'
    except NameError:
        # If NameError occurs, it means the class is not found
        pass

    return 'unknown'

words = ['math', 'yaml','sklearn']
def has_classes(module_name):
    try:
        # Import the module
        module = importlib.import_module(module_name)
    except ImportError:
        return False, []

    # Find all classes in the module
    classes = [name for name, obj in inspect.getmembers(module, inspect.isclass) if obj.__module__ == module_name]
    return bool(classes), classes

for word in words:
    result = is_module_or_class(word)

    print(f"{word} is a {result}")

    if result == 'module':
        print(has_classes(word))
    print(dir(word))