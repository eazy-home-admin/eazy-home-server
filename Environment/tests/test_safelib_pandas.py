import unittest
import os
import pandas as pd
import tempfile
from unittest.mock import patch
from Environment.SafeLibs import SafePandasDataFrame
import builtins

class TestSafePandasDataFrame(unittest.TestCase):

    def setUp(self):
        # Create a temporary directory for allowed paths
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.allowed_dirs = [self.tmp_dir.name]
        self.test_csv_path = os.path.join(self.tmp_dir.name, 'test.csv')
        self.test_excel_path = os.path.join(self.tmp_dir.name, 'test.xlsx')
        self.test_json_path = os.path.join(self.tmp_dir.name, 'test.json')

        # Create test files
        df = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]})
        df.to_csv(self.test_csv_path, index=False)
        df.to_excel(self.test_excel_path, index=False)
        df.to_json(self.test_json_path)

    def tearDown(self):
        # Cleanup the temporary directory
        self.tmp_dir.cleanup()

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_read_csv_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.return_value = True

        df = SafePandasDataFrame.read_csv(self.test_csv_path, self.allowed_dirs)
        self.assertTrue(isinstance(df, SafePandasDataFrame))
        self.assertEqual(df.shape, (3, 2))

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_read_csv_not_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.return_value = False
        print(builtins)
        with self.assertRaises(PermissionError):
            SafePandasDataFrame.read_csv(self.test_csv_path, self.allowed_dirs)

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_read_excel_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.return_value = True

        df = SafePandasDataFrame.read_excel(self.test_excel_path, self.allowed_dirs)
        self.assertTrue(isinstance(df, SafePandasDataFrame))
        self.assertEqual(df.shape, (3, 2))

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_read_excel_not_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.return_value = False

        with self.assertRaises(PermissionError):
            SafePandasDataFrame.read_excel(self.test_excel_path, self.allowed_dirs)

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_read_json_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.return_value = True

        df = SafePandasDataFrame.read_json(self.test_json_path, self.allowed_dirs)
        self.assertTrue(isinstance(df, SafePandasDataFrame))
        self.assertEqual(df.shape, (3, 2))

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_read_json_not_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.return_value = False

        with self.assertRaises(PermissionError):
            SafePandasDataFrame.read_json(self.test_json_path, self.allowed_dirs)

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_to_csv_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.return_value = True

        df = SafePandasDataFrame.read_csv(self.test_csv_path, self.allowed_dirs)
        output_csv_path = os.path.join(self.tmp_dir.name, 'output.csv')
        df.to_csv(output_csv_path, index=False)
        self.assertTrue(os.path.exists(output_csv_path))
        os.remove(output_csv_path)

    @patch('Environment.SafeLibs.SafePandasDataFrame.acl_manager', create=True)
    def test_to_csv_not_allowed(self, mock_acl_manager):
        mock_acl_manager.is_path_allowed.side_effect = lambda path: path.startswith(self.tmp_dir.name)

        df = SafePandasDataFrame.read_csv(self.test_csv_path, self.allowed_dirs)
        output_csv_path = "/not_allowed/path/output.csv"
        with self.assertRaises(PermissionError):
            df.to_csv(output_csv_path, index=False)


if __name__ == "__main__":
    unittest.main()
