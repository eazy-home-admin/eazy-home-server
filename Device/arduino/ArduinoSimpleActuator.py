from Device.core.device import Device, RequiredArgs


class ArduinoSimpleActuator(Device):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.category = "Actuator"
        self.is_controller = False
        self.pin = kwargs.get('pin', None)
        self.controller_id = kwargs.get('controller_id', None)
        if self.pin is None:
            raise ValueError('Pin Number is expected, Check the database entry.')
        if self.controller_id is None:
            raise ValueError('Controller ID is expected, Check the database entry.')

    @staticmethod
    def required_arguments():
        return [
            RequiredArgs(name='pin', description='Control Pin Number', required=True),
            RequiredArgs(name='monitor_pin', description='Pin to monitor the state/power the actuator receives. '
                                                         'Optional.', default_value='None', required=False),
            RequiredArgs(name='state', description='State', default_value='off', required=True),
            RequiredArgs(name='power_consumption', description='Estimated Power Consumption in hours',
                         default_value='None', required=False)
        ]


