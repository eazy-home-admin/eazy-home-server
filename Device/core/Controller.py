from abc import ABC, abstractmethod

from Device.core.device import Device, Register, RequiredArgs


class Controller(ABC, Device):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.is_controller = True

    @abstractmethod
    @Register.ignore
    def initialize_connection(self, force_reconnect=False):
        """
        Initializes the connection to the controller. Implementations of this method should
        establish any necessary connections or setups required for the controller to function.
        """
        pass

    @abstractmethod
    @Register.ignore
    def close_connection(self):
        """
        Closes the connection to the controller. Implementations of this method should
        safely terminate any connections or cleanup resources used by the controller.
        """
        pass

    @abstractmethod
    def write(self, data_string, args: dict = None):
        pass

    @abstractmethod
    def read(self, data_string, args: dict = None):
        pass

    @staticmethod
    @Register.ignore
    def required_arguments():
        """
        Defines the required arguments for controller registration. This method can be overridden by
        subclasses to specify the necessary data for creating an instance of a specific controller type.
        """
        return super().required_arguments() + [
            RequiredArgs('is_controller', 'Indicator to check if the controller', True, True)
        ]


class ControllerInitializationFailed(Exception):
    pass
