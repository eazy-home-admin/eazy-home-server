import os

import nmap


class DeviceScanner:

    def __init__(self, network_id):
        self.network_id = network_id
        self._devices_list = []

    def scan_network(self, ip_range):
        nm = nmap.PortScanner()
        nm.scan(hosts=ip_range, arguments='-sn')
        hosts_list = [(x, nm[x]['status']['state'], nm[x].hostname()) for x in nm.all_hosts()]
        for host, status, hostname in hosts_list:
            # Store in device list as a dictionary
            self._devices_list.append({
                'ip': host,
                'status': status,
                'hostname': hostname if hostname else 'Unknown'
            })
            print(f'{host}\t{status}\t{hostname}')

    def get_device_list(self):
        return self._devices_list


# if __name__ == '__main__':
#     devices = DeviceScanner('192.168.100.15')
#     print(devices.scan_network('192.168.100.0-255'))
#     print(devices.get_device_list())
