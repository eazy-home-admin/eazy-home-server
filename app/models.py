"""
Data models used by eazy-home-server.
"""
import os.path

import yaml


class config:
    def __init__(self):
        # Doo
        self.debug = True
        self.port = 5000
        self.set_https = False
        self.enable_telegram = False



    def load_config(self):

        # Directory is at Home/$USER/.config/eazy-home/config.yaml
        location =  "~/.config/eazy-home/config.yaml"
        # check if exist
        if os.path.exists(location):
            # load the yaml file and unpack it
            with open(location,'r') as f:
                x = yaml.safe_load(f)
                self.debug = x['debug']
                self.port = x['port']
                self.set_https = x['set_https']
                self.enable_telegram = x['enable_telegram']

        # load
        else:
            # create
            with open(location, 'w+') as f:
                yaml.dump(self.to_dict(), f)
            # write config

    def save(self):
        with open('config.yaml', 'w') as f:
            yaml.dump(self.to_dict(), f)

    def update_config(self,data:dict):
        self.debug = data.get('debug', self.debug)
        self.port = data.get('port', self.port)
        self.set_https = data.get('set_https', self.set_https)
        self.enable_telegram = data.get('enable_telegram', self.enable_telegram)
        self.save()

    def to_dict(self):
        return {
            "debug": self.debug,
            "port": self.port,
            "set_https": self.set_https,
            "enable_telegram": self.enable_telegram
        }

    @staticmethod
    def get_schema():
        return {
            "debug": {
                "type": "boolean",
                "required": True
            },
            "port": {
                "type": "integer",
                "required": True
            },
            "set_https": {
                "type": "boolean",
                "required": True
            },
            "enable_telegram": {
                "type": "boolean",
                "required": True
            }

class User:
    def __init__(self):
        self.user_id = None
        self.username = None
        self.password = None
        self.roles = None

    @staticmethod
    def get_schema():
        return {
            "user_id": {
                "type": "string",
                "required": True,
                "unique": True
            },
            "username": {
                "type": "string",
                "required": True,
                "unique": True
            },
            "password": {
                "type": "string",
                "required": True
            },
            "roles": {
                "type": "string",
                "required": False,
                'default':'user'
            }

        }

