# Data Model for Eazy Home

## Objectives

- **Cater to the Needs of Home Automation:**
  - The data model is designed to accommodate various types of devices within a home automation system, ensuring compatibility and ease of integration.
  
- **Flexible Data Acceptance:**
  - The model accepts and stores data for any device, providing on-demand access to programmatically interact with device configurations and statuses.

- **Backward Compatibility:**
  - The data model is structured to avoid breaking existing devices when new devices or attributes are added, ensuring a seamless transition and integration process.

- **Standardization of Data Models:**
  - To maintain consistency and reliability across the home automation ecosystem, the data model follows standardized formats, promoting interoperability and ease of use.

## Potential Challenges

- **Diverse Data:**
  - Managing a wide variety of devices, each with its own unique set of attributes and requirements, poses a challenge in maintaining a cohesive and unified data model.

- **Security Issues:**
  - Opening APIs for device configuration and addition can introduce security vulnerabilities. Ensuring secure communication and access control mechanisms is critical to prevent unauthorized access and tampering.

## Data Model Structure

### 1. Zones

Zones are logical groupings of devices based on their physical locations (e.g., rooms) or functional areas within the home.

```yaml
zones:
  - name: 'Hall'
    id: Z1
  - name: 'Bedroom'
    id: Z2
  - name: 'Kitchen'
    id: Z3
```

### 2. Devices

Devices are categorized into controllers, actuators, sensors, standalone actuators, and mixed devices. Each device has a unique ID and is associated with specific attributes that define its functionality.

#### Common Attributes

- **name:** The human-readable name of the device.
- **id:** A unique identifier, prefixed according to the device type (`N` for non-controller actuators, `C` for controllers, `S` for sensors, `A` for standalone actuators, `M` for mixed devices).
- **type:** The device type (e.g., actuator, controller, sensor).
- **is_controller:** Boolean flag indicating whether the device is a controller.
- **module:** The software module used to control the device.
- **zone_id:** The zone to which the device belongs.
- **attributes:** A dictionary containing specific device-related information.
- **health_check:** A section dedicated to monitoring the device's status.

#### Example Device Entry

```yaml
devices:
  1:
    name: lamp1
    id: N1
    type: actuator
    is_controller: False
    module: 'arduino_lamp'
    room: 'Hall'
    zone_id: Z1
    updated_on: 2023-12-11 22:00:57.491205
    usage: 2957.433333333334
    controller: C1
    attributes:
      pin: 13
      status: 'off'
      power_usage: 0.4  # Power usage per hour in KW/h
    health_check:
      last_check: 2024-08-23 10:00:00
      status: 'healthy'
      issues:
        - None
```

### 3. Health Checks

Health checks are essential for monitoring the operational status of each device. The data model includes a dedicated `health_check` section for every device, capturing:

- **last_check:** Timestamp of the last health check performed on the device.
- **status:** The overall health status (`healthy`, `warning`, or `critical`).
- **issues:** A list of issues detected during the health check. If there are no issues, this can be set to `None`.

### 4. Attribute Flexibility

Each device's attributes are stored under the `attributes` section. This allows for dynamic and flexible storage of device-specific information without needing to modify the core data structure. It supports diverse data types and custom attributes tailored to each device's requirements.

#### Example of Diverse Attributes

```yaml
attributes:
  pin:
    - 12
    - 13
    - 14
  R: 200
  G: 120
  B: 123
  A: 100
  status: 'off'
  light_type: 'christmas'
```

### 5. Controller and Controlled Devices

Controllers manage other devices and are identified by the `is_controller: True` flag. Controlled devices are listed under the `controlled_devices` attribute, linking the controller to its associated devices.

#### Example of Controller Entry

```yaml
2:
  name: arduino1
  id: C1
  type: controller
  is_controller: True
  module: 'ArduinoController'
  zone_id: Z1
  controlled_devices:
    - N1
    - N2
    - N3
  attributes:
    controller_connection_type: Serial
    port: '/dev/tty_ACM0'
    baud: 9600
    timeout: 1
    power_usage: 0.4  # Power usage per hour in KW/h
  health_check:
    last_check: 2024-08-23 10:00:00
    status: 'warning'
    issues:
      - 'High latency in serial communication'
      - 'Temperature threshold exceeded'
```

## Information on Data

### Data Accessibility

The data model is designed for flexibility and accessibility, allowing devices to be added or updated without affecting the overall system. Data is structured in a way that any program interacting with the home automation system can easily access and manipulate the information as needed.

### Data Integrity and Validation

To maintain data integrity, the system should incorporate validation mechanisms ensuring that all required fields are present and correctly formatted when a device is added or updated. Attributes should be validated based on the device type, and health checks should be regularly performed to keep the data up-to-date.

### Security Considerations

Given the potential security risks associated with opening APIs for device management, the following security measures are recommended:

- **Authentication and Authorization:** Ensure that only authenticated users or systems can add, modify, or delete devices in the data model.
- **Data Encryption:** Encrypt sensitive data, such as API tokens and device communications, to protect against unauthorized access.
- **Audit Logging:** Maintain logs of all changes made to the data model, including who made the change and when, to track and identify any potential security breaches.

### Scalability

The data model is scalable, allowing it to grow with the addition of new devices, controllers, and zones. The flexible attribute storage ensures that new types of devices can be integrated without requiring structural changes to the data model.

### Future Enhancements

- **Real-time Monitoring:** Incorporate real-time monitoring and alerts for critical device health issues.
- **Advanced Analytics:** Enable analytics on device usage, health trends, and system performance.
- **Integration with External Systems:** Facilitate integration with external home automation ecosystems and IoT platforms.

